/*
 * Authored by Teedit
 * Created July 2020
 * Developed to prove Barbara's fault in her rotation.
*/

#include <iostream>
#include "Core.hpp"
#include "DemoLock.hpp"



void InitState(State* s) {
    s->stats->crit = 0;
    s->stats->distance = 40;
    s->stats->haste = 2900;
    s->stats->hit = 1742;
    s->stats->intel = 7835;
    s->stats->castSpeed = 1.f;
    s->stats->mastery = 0;
    s->stats->spellpower = 2700;
    s->stats->bonusDamage = 1.f;
    s->stats->bonusSpellpower = 1.f;
    s->stats->bonusHaste = 1.f;
    s->stats->bonusDoT = 1.f;
}

int main()
{
    State* s = new State();
    InitState(s);
    Spell** spells = InitDemoSpells();
    //s->AddBuff(new HunterPet());
    s->AddBuff(new ShadowBiteCD());
    std::cout << DemoLockRecursive(s, spells, 4.f) << std::endl;
    for (int i = 0; i < DEMO_SPELL_COUNT; ++i) {
        delete spells[i];
    }
    delete[] spells;
    s->Clean();
    delete s;
    RNG::Clean();
}