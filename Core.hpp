/*
 * Authored by Teedit
 * Created July 2020
 * Developed to prove Barbara's fault in her rotation.
*/

#ifndef CORE_HPP
#define CORE_HPP
#include <queue>
#include <string>
#include <random>
#include <chrono>
#include <iostream>

class State; class Buff;

struct Event {
	float time = 0;
	Buff* buff = nullptr;
};

class Buff {
public:
	std::string name;
	bool overwrites = true;

	virtual void Effect(State* s) {}
	virtual Event* GetNextEvent(State* s) { return nullptr; }
	virtual void OnStart(State* s) {}
	virtual void OnEnd(State* s) {}
	virtual void OnReapplied(State* s) {}
	virtual Buff* Duplicate() { return nullptr; }
	virtual void Clean() {}
};

class EventComparator {
public:
	bool operator()(const Event* l, const Event* r) const { return l->time > r->time; }
};

struct Stats {
	int intel, hit, haste, crit, mastery, spellpower;
	float distance, bonusDamage, bonusSpellpower, castSpeed, bonusHaste, bonusDoT;

	void operator=(Stats& other) {
		intel = other.intel;
		hit = other.hit;
		haste = other.haste;
		crit = other.crit;
		mastery = other.mastery;
		spellpower = other.spellpower;
		castSpeed = other.castSpeed;
		bonusDamage = other.bonusDamage;
		bonusSpellpower = other.bonusSpellpower;
		distance = other.distance;
	}

	void operator=(Stats* other) {
		intel = other->intel;
		hit = other->hit;
		haste = other->haste;
		crit = other->crit;
		mastery = other->mastery;
		spellpower = other->spellpower;
		castSpeed = other->castSpeed;
		bonusDamage = other->bonusDamage;
		bonusSpellpower = other->bonusSpellpower;
		distance = other->distance;
	}

	inline int SpellDamage() { return (intel + spellpower + 628) * 1.1f * bonusSpellpower; }
	inline float SpellHaste() { return haste * bonusHaste; }
	inline float CastSpeed() { return SpellHaste() / 12800.f + castSpeed; }
};

class State {
	float time;
	long dmg;
	std::priority_queue<Event*, std::vector<Event*>, EventComparator> q;
	float gcd;
	std::vector<Buff*> buffs;
	State* fork;
	float probability;

public:
	Stats* stats;

	State() :
		dmg(0L),
		time(0.f),
		gcd(0.f),
		q(std::priority_queue<Event*, std::vector<Event*>, EventComparator>()),
		buffs(std::vector<Buff*>()),
		fork(nullptr),
		probability(1) { stats = new Stats(); }

	Event* GetEvent();

	void AddBuff(Buff* buff);
	Buff* HasBuff(std::string name);
	void RemoveBuff(std::string name);
	void ClearEvents(std::string name);
	void Relink(Buff* buff);
	State* Duplicate();
	void Clean();

	void SetGCD() {
		float mod = stats->CastSpeed();
		if (mod >= 2.f) gcd = 0.75f;
		else gcd = 1.5f / mod;
		Event* e = new Event();
		e->buff = nullptr;
		e->time = time + gcd;
		AddEvent(e);
	}
	float GetGCD() { return gcd; }
	void AddEvent(Event* e) { q.push(e); }
	long GetDmg() { return dmg; }
	void AddDmg(int d) { dmg += d; }
	float GetTime() { return time; }
	bool NoEvents() { return q.empty(); }
	inline void AddFork(State* s, float prob) { fork = s; probability = prob; }
	inline bool HasFork() { return fork != nullptr; }
	inline State* GetFork() { State* ret = fork; fork = nullptr; return ret; }
	inline float GetProbability() { float ret = probability; probability = 0; return ret; }
};

class TestBuff : public Buff {
public:
	TestBuff() { name = "TestBuff"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class TestDebuff : public Buff {
	int ticks = 0;
	bool reapplied = false;
public:
	TestDebuff() { 
		name = "TestDebuff";
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class TestProc : public Buff {
public:
	TestProc() { name = "TestProc"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class Spell {
public:
	std::string name;
	virtual bool CanCast(State* s) { return true; }
	virtual void Cast(State* s) {}
	virtual void Invoke(State* s, Stats& stats) {}
	virtual void Arrive(State* s, Stats& stats) {}
};

class Casting : public Buff {
	float duration = 0;
	Spell* spell;
	Stats stats;
public:
	Casting(float length, Spell* spell, Stats* s):duration(length),spell(spell) {
		name = "Casting";
		stats = s;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class Travelling : public Buff {
	Spell* spell;
	float duration = 0.f;
	Stats _stats;
public:
	Travelling(Spell* spell, float duration, Stats& stats) :spell(spell), duration(duration) {
		name = "Travelling";
		overwrites = false;
		_stats = stats;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class RNG {
	std::default_random_engine* gen;
	std::uniform_int_distribution<int>* dist;
	static RNG* instance;
	RNG() {
		gen = new std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());
		dist = new std::uniform_int_distribution<int>(0, 100000);
	}

public:
	static RNG* GetInstance() {
		if (!instance) instance = new RNG();
		return instance;
	}

	float Number() {
		return (*dist)(*gen) * 0.001f;
	}

	static void Clean() {
		if (instance == nullptr) return;
		delete instance->gen;
		delete instance->dist;
		delete instance;
	}
};

#endif