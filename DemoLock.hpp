/*
 * Authored by Teedit
 * Created July 2020
 * Developed to prove Barbara's fault in her rotation.
*/

#ifndef DEMOLOCK_HPP
#define DEMOLOCK_HPP

#include "Core.hpp"

enum DemoSpells {IMMOLATE = 0, BANE_OF_DOOM, BANE_OF_AGONY, CORRUPTION, SHADOWFLAME, HAND_OF_GULDAN, INCINERATE, SHADOW_BOLT, SOUL_FIRE, CURSE_OF_ELEMENTS, SHADOW_BITE, HUNTER_MELEE, GUARD_MELEE, LEGION_STRIKE, FELSTORM, SOULBURN, DEMON_SOUL, METAMORPHOSIS, SUMMON_HUNTER, SUMMON_GUARD, DEMO_SPELL_COUNT};

class ShadowBolt : public Spell {
public:
	ShadowBolt() {
		name = "ShadowBolt";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class Incinerate : public Spell {
public:
	Incinerate() {
		name = "Incinerate";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class Immolate : public Spell {
public:
	Immolate() {
		name = "Immolate";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class ShadowAndFlame : public Buff {
public:
	ShadowAndFlame() { name = "ShadowAndFlame"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class ImmolateDoT : public Buff {
	int tick;
	int ticks_left;
	float tick_len;
	float crit_chance;
	ImmolateDoT(ImmolateDoT* other) {
		name = "ImmolateDoT";
		tick = other->tick;
		ticks_left = other->ticks_left;
		tick_len = other->tick_len;
		crit_chance = other->crit_chance;
	}
public:
	ImmolateDoT(Stats& stats) {
		name = "ImmolateDoT";
		tick = (422 + 0.17595f * stats.SpellDamage()) * 1.2f * 1.15f * 1.1f * 1.02f * stats.bonusDamage * stats.bonusDoT;
		if (stats.SpellHaste() >= 2739) ticks_left = 9;
		else if (stats.SpellHaste() >= 896) ticks_left = 8;
		else ticks_left = 7;
		tick_len = 21.f / ticks_left;
		crit_chance = stats.crit / 17900.f + stats.intel / 64900.f + 0.017f;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;

	inline int Ticks_left() { return ticks_left; }
	int Tick() { return tick; }
};

class MoltenCore : public Buff {
	int stack;
	MoltenCore(int stacks) {
		name = "MoltenCore";
		stack = stacks;
	}
public:
	MoltenCore() {
		name = "MoltenCore";
		stack = 3;
	}

	void Effect(State * s) override;
	Event* GetNextEvent(State * s) override;
	void OnStart(State * s) override;
	void OnEnd(State * s) override;
	void OnReapplied(State * s) override;
	Buff* Duplicate() override;
	void Clean() override;

	void ConsumeStack(State* s) {
		if (--stack == 0) {
			s->ClearEvents(name);
			OnEnd(s);
		}
	}
};

class CurseOfElements : public Spell {
public:
	CurseOfElements() {
		name = "CurseOfElements";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class CurseOfElementsBuff : public Buff {
public:
	CurseOfElementsBuff() { name = "CurseOfElementsBuff"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class BaneOfDoom : public Spell {
public:
	BaneOfDoom() {
		name = "BaneOfDoom";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class BaneOfDoomDoT : public Buff {
	int tick;
	int ticks_left;
	float crit_chance;
	BaneOfDoomDoT(BaneOfDoomDoT* other) {
		name = "BaneOfDoomDoT";
		tick = other->tick;
		ticks_left = other->ticks_left;
		crit_chance = other->crit_chance;
	}
public:
	BaneOfDoomDoT(Stats& stats) {
		name = "BaneOfDoomDoT";
		tick = (1947 + 0.88f * stats.SpellDamage()) * 1.15f * 1.02f * stats.bonusDamage * stats.bonusDoT;
		ticks_left = 4;
		crit_chance = stats.crit / 17900.f + stats.intel / 64900.f + 0.017f;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;

	int TicksLeft() { return ticks_left; }
	int Tick() { return tick; }
};

class BaneOfAgony : public Spell {
public:
	BaneOfAgony() {
		name = "BaneOfAgony";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class BaneOfAgonyDoT : public Buff {
	int tick;
	int ticks_left;
	int ticks_total;
	float tick_len;
	float crit_chance;
	int mult;
	BaneOfAgonyDoT(BaneOfAgonyDoT* other) {
		name = "BaneOfAgonyDoT";
		tick = other->tick;
		ticks_left = other->ticks_left;
		ticks_total = other->ticks_total;
		tick_len = other->tick_len;
		crit_chance = other->crit_chance;
		mult = other->mult;
	}
public:
	BaneOfAgonyDoT(Stats& stats) {
		name = "BaneOfAgonyDoT";
		tick = (63 + 0.044f * stats.SpellDamage()) * 1.15f * 1.02f * stats.bonusDamage * stats.bonusDoT;
		if (stats.SpellHaste() / 128.f >= 24.f) {
			ticks_left = 5;
			ticks_total = 15;
		}
		else if (stats.SpellHaste() / 128.f >= 14.f) {
			ticks_left = 4;
			ticks_total = 14;
		}
		else if (stats.SpellHaste() / 128.f >= 4.f) {
			ticks_left = 4;
			ticks_total = 13;
		}
		else {
			ticks_left = 4;
			ticks_total = 12;
		}
		tick_len = 24.f / ticks_total;
		crit_chance = stats.crit / 17900.f + stats.intel / 64900.f + 0.017f;
		mult = 1;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;

	int TicksLeft() { return ticks_left; }
	int Tick() { return tick; }
};

class Corruption : public Spell {
public:
	Corruption() {
		name = "Corruption";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class CorruptionDoT : public Buff {
	int tick;
	int ticks_left;
	float crit_chance;
	float tick_len;
	CorruptionDoT(CorruptionDoT* other) {
		name = "CorruptionDoT";
		tick = other->tick;
		ticks_left = other->ticks_left;
		crit_chance = other->crit_chance;
		tick_len = other->tick_len;
	}
public:
	CorruptionDoT(Stats& stats) {
		name = "CorruptionDoT";
		tick = (147 + 0.17595f * stats.SpellDamage()) * 1.15f * 1.02f * stats.bonusDamage * stats.bonusDoT;
		if (stats.SpellHaste() >= 3200) ticks_left = 8;
		else if (stats.SpellHaste() >= 1024) ticks_left = 7;
		else ticks_left = 6;
		crit_chance = stats.crit / 17900.f + stats.intel / 64900.f + 0.017f;
		tick_len = 19.f / ticks_left;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;

	int TicksLeft() { return ticks_left; }
	int Tick() { return tick; }
};

class ShadowTrance : public Buff {
public:
	ShadowTrance() { name = "ShadowTrance"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;

	void Use(State* s);
};

class ShadowFlame : public Spell {
public:
	ShadowFlame() {
		name = "ShadowFlame";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class ShadowFlameDoT : public Buff {
	int tick;
	int ticks_left;
	float crit_chance;
	float tick_len;
	ShadowFlameDoT(ShadowFlameDoT* other) {
		name = "ShadowFlameDoT";
		tick = other->tick;
		ticks_left = other->ticks_left;
		crit_chance = other->crit_chance;
		tick_len = other->tick_len;
	}
public:
	ShadowFlameDoT(Stats& stats) {
		name = "ShadowFlameDoT";
		tick = (162 + 0.2f * stats.SpellDamage()) * 1.15f * 1.02f * stats.bonusDamage * stats.bonusDoT;
		if (stats.SpellHaste() / 128.f >= 16.5f) ticks_left = 4;
		else ticks_left = 3;
		crit_chance = stats.crit / 17900.f + stats.intel / 64900.f + 0.017f;
		tick_len = 6.f / ticks_left;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class ShadowFlameCD : public Buff {
public:
	ShadowFlameCD() { name = "ShadowFlameCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class HandOfGuldan : public Spell {
public:
	HandOfGuldan() {
		name = "HandOfGuldan";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class HandOfGuldanCD : public Buff {
public:
	HandOfGuldanCD() { name = "HandOfGuldanCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class CurseOfGuldan : public Buff {
public:
	CurseOfGuldan() { name = "CurseOfGuldan"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class SoulFire : public Spell {
public:
	SoulFire() {
		name = "SoulFire";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class ShadowBite : public Spell {
public:
	ShadowBite() {
		name = "ShadowBite";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class ShadowBiteCD : public Buff {
public:
	ShadowBiteCD() { name = "ShadowBiteCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class HunterMelee : public Spell {
public:
	HunterMelee() {
		name = "HunterMelee";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class HunterMeleeCD : public Buff {
public:
	HunterMeleeCD() { name = "HunterMeleeCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class GuardMelee : public Spell {
public:
	GuardMelee() {
		name = "GuardMelee";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class GuardMeleeCD : public Buff {
public:
	GuardMeleeCD() { name = "GuardMeleeCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class GuardPet : public Buff {
public:
	GuardPet() { name = "GuardPet"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class HunterPet : public Buff {
public:
	HunterPet() { name = "HunterPet"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class LegionStrike : public Spell {
public:
	LegionStrike() {
		name = "LegionStrike";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class LegionStrikeCD : public Buff {
public:
	LegionStrikeCD() { name = "LegionStrikeCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class FelStorm : public Spell {
public:
	FelStorm() {
		name = "FelStorm";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class FelStormDoT : public Buff {
	int tick;
	int ticks_left;
	float crit_chance;
	FelStormDoT(FelStormDoT* other) {
		name = "FelStormDoT";
		tick = other->tick;
		ticks_left = other->ticks_left;
		crit_chance = other->crit_chance;
	}
public:
	FelStormDoT(Stats& stats) {
		name = "FelStormDoT";
		tick = (1107 + 0.45845f * stats.SpellDamage() * 1.1f) * (1.18f + stats.mastery * 2.3f / 17900.f);
		ticks_left = 6;
		crit_chance = stats.crit / 17900.f + stats.intel / 64900.f + 0.017f;
	}

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class FelStormCD : public Buff {
public:
	FelStormCD() { name = "FelStormCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class Soulburn : public Spell {
public:
	Soulburn() {
		name = "Soulburn";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class SoulburnBuff : public Buff {
public:
	SoulburnBuff() { name = "SoulburnBuff"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class TemporalRuin : public Buff {
public:
	TemporalRuin() { name = "TemporalRuin"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class SoulburnCD : public Buff {
public:
	SoulburnCD() { name = "SoulburnCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class Soulshard : public Buff {
	int shards;
public:
	Soulshard():shards(3) { name = "Soulshard"; }
	Soulshard(Soulshard* other):shards(other->shards) { name = "Soulshard"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;

	void UseShard(State* s) {
		if (--shards == 0) {
			OnEnd(s);
		}
	}
};

class DemonSoul : public Spell {
public:
	DemonSoul() {
		name = "DemonSoul";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class DemonSoulBuff : public Buff {
	char demon;
public:
	DemonSoulBuff() { name = "DemonSoulBuff"; }
	DemonSoulBuff(DemonSoulBuff* other) :DemonSoulBuff() { demon = other->demon; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class DemonSoulCD : public Buff {
public:
	DemonSoulCD() { name = "DemonSoulCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class Metamorphosis : public Spell {
public:
	Metamorphosis() {
		name = "Metamorphosis";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class MetamorphosisBuff : public Buff {
	float bonusDmg;
public:
	MetamorphosisBuff() { name = "MetamorphosisBuff"; }
	MetamorphosisBuff(MetamorphosisBuff* other) :MetamorphosisBuff() { bonusDmg = other->bonusDmg; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;
};

class MetamorphosisCD : public Buff {
	float expiration;
public:
	MetamorphosisCD() { name = "MetamorphosisCD"; }
	MetamorphosisCD(float exp): expiration(exp) { name = "MetamorphosisCD"; }

	void Effect(State* s) override;
	Event* GetNextEvent(State* s) override;
	void OnStart(State* s) override;
	void OnEnd(State* s) override;
	void OnReapplied(State* s) override;
	Buff* Duplicate() override;
	void Clean() override;

	void SubCD(State* s);
};

class SummonHunter : public Spell {
public:
	SummonHunter() {
		name = "SummonHunter";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

class SummonGuard : public Spell {
public:
	SummonGuard() {
		name = "SummonGuard";
	}

	bool CanCast(State* s) override;
	void Cast(State* s) override;
	void Invoke(State* s, Stats& stats) override;
	void Arrive(State* s, Stats& stats) override;
};

long DemoLockRecursive(State* s, Spell** spells, float maxTime);

long SmartDemoLock(State* s, Spell** spells, float maxTime);

Spell** InitDemoSpells();

#endif