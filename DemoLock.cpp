/*
 * Authored by Teedit
 * Created July 2020
 * Developed to prove Barbara's fault in her rotation.
*/

#include "DemoLock.hpp"
#include <iostream>

Spell** InitDemoSpells() {
	Spell** ret = new Spell*[DEMO_SPELL_COUNT];
	ret[0] = new Immolate();
	ret[1] = new BaneOfDoom();
	ret[2] = new BaneOfAgony();
	ret[3] = new Corruption();
	ret[4] = new ShadowFlame();
	ret[5] = new HandOfGuldan();
	ret[6] = new Incinerate();
	ret[7] = new ShadowBolt();
	ret[8] = new SoulFire();
	ret[9] = new CurseOfElements();
	ret[10] = new ShadowBite();
	ret[11] = new HunterMelee();
	ret[12] = new GuardMelee();
	ret[13] = new LegionStrike();
	ret[14] = new FelStorm();
	ret[15] = new Soulburn();
	ret[16] = new DemonSoul();
	ret[17] = new Metamorphosis();
	ret[18] = new SummonHunter();
	ret[19] = new SummonGuard();
	return ret;
}

inline bool Miss(Stats& s, float ran) {
	float hit = 83.f + s.hit / 102.45f;
	return ran >= hit;
}

inline void MissEffect(Stats& s, float& dmg) {
	if (s.hit < 1742) dmg *= 0.83f + s.hit / 10245.f;
}

inline bool Crit(Stats& s, float ran) {
	float crit = s.crit / 179.f + s.intel / 649.f + 1.7f;
	return ran < crit;
}

inline void CritEffect(Stats& s, float& dmg, float bonusCrit) {
	dmg *= 1 + 1.03f * (s.crit / 17900.f + s.intel / 64900.f + 0.017f + bonusCrit);
}

inline void CritEffect(float setChance, float& dmg, float bonusCrit) {
	dmg *= 1 + 1.03f * (setChance + bonusCrit);
}

inline float CastTime(Stats& s, float base) {
	return base / s.CastSpeed();
}

bool ShadowBolt::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void ShadowBolt::Cast(State* s) {
	if (s->HasBuff("ShadowTrance")) {
		s->ClearEvents("ShadowTrance");
		s->RemoveBuff("ShadowTrance");
		s->AddBuff(new Travelling(this, s->stats->distance * 0.1f, *s->stats));
	}
	else s->AddBuff(new Casting(CastTime(*s->stats, 2.5f), this, s->stats));
	s->SetGCD();
}

void ShadowBolt::Invoke(State* s, Stats& stats) {
	s->AddBuff(new Travelling(this, stats.distance * 0.1f, stats));
}

void ShadowBolt::Arrive(State* s, Stats& stats) {
	float dmg = (616 + stats.SpellDamage() * 0.7548f) * 1.15f * 1.12f * 1.02f * stats.bonusDamage;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(stats, dmg);
	CritEffect(stats, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	Buff* b = new ShadowAndFlame();
	s->AddBuff(b);
	if (s->HasBuff("MetamorphosisCD")) {
		State* ns = s->Duplicate();
		((MetamorphosisCD*)(ns->HasBuff("MetamorphosisCD")))->SubCD(ns);
		s->AddFork(ns, 0.15f);
	}
}

void ShadowAndFlame::Effect(State* s) {
	OnEnd(s);
}

Event* ShadowAndFlame::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 30.f;
	e->buff = this;
	return e;
}

void ShadowAndFlame::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void ShadowAndFlame::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void ShadowAndFlame::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* ShadowAndFlame::Duplicate() {
	return new ShadowAndFlame();
}

void ShadowAndFlame::Clean() {}

bool Incinerate::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void Incinerate::Cast(State* s) {
	Casting* c;
	if (s->HasBuff("MoltenCore")) {
		int oldBonus = s->stats->bonusDamage;
		s->stats->bonusDamage *= 1.18f;
		c = new Casting(CastTime(*s->stats, 2.25f), this, s->stats);
		s->stats->bonusDamage = oldBonus;
	}
	else c = new Casting(CastTime(*s->stats, 2.25f), this, s->stats);
	s->AddBuff(c);
	s->SetGCD();
}

void Incinerate::Invoke(State* s, Stats& stats) {
	s->AddBuff(new Travelling(this, stats.distance * 0.1f, stats));
	MoltenCore* mc;
	if (mc = (MoltenCore*)s->HasBuff("MoltenCore")) mc->ConsumeStack(s);
}

void Incinerate::Arrive(State* s, Stats& stats) {
	float dmg = (553 + stats.SpellDamage() * 0.53876f) * 1.15f * 1.12f * 1.02f * stats.bonusDamage;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(stats, dmg);
	CritEffect(stats, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	Buff* b = new ShadowAndFlame();
	s->AddBuff(b);
	if (s->HasBuff("MetamorphosisCD")) {
		State* ns = s->Duplicate();
		((MetamorphosisCD*)(ns->HasBuff("MetamorphosisCD")))->SubCD(ns);
		s->AddFork(ns, 0.15f);
	}
}

bool Immolate::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void Immolate::Cast(State* s) {
	s->AddBuff(new Casting(CastTime(*s->stats, 1.5f), this, s->stats));
	s->SetGCD();
}

void Immolate::Invoke(State* s, Stats& stats) {
	float dmg = (665 + stats.SpellDamage() * 0.22f) * 1.15f * 1.02f * 1.2f * stats.bonusDamage;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(stats, dmg);
	CritEffect(stats, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	Buff* b = new ImmolateDoT(stats);
	s->AddBuff(b);
}

void Immolate::Arrive(State* s, Stats& stats) {}

void ImmolateDoT::Effect(State* s) {
	float dmg = tick;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	CritEffect(crit_chance, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	if (--ticks_left == 0) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
	State* ns = s->Duplicate();
	ns->AddBuff(new MoltenCore());
	s->AddFork(ns, 0.06f);
	if (--ticks_left == 0) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
}

Event* ImmolateDoT::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + tick_len;
	e->buff = this;
	return e;
}

void ImmolateDoT::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void ImmolateDoT::OnReapplied(State* s) {
	++ticks_left;
	s->Relink(this);
}

void ImmolateDoT::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* ImmolateDoT::Duplicate() {
	return new ImmolateDoT(this);
}

void ImmolateDoT::Clean() {}

void MoltenCore::Effect(State* s) {
	OnEnd(s);
}

Event* MoltenCore::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 15.f;
	e->buff = this;
	return e;
}

void MoltenCore::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void MoltenCore::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void MoltenCore::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* MoltenCore::Duplicate() {
	return new MoltenCore(stack);
}

void MoltenCore::Clean() {}

bool CurseOfElements::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void CurseOfElements::Cast(State* s) {
	s->AddBuff(new CurseOfElementsBuff());
	s->SetGCD();
}

void CurseOfElements::Invoke(State* s, Stats& stats) {}

void CurseOfElements::Arrive(State* s, Stats& stats) {}

void CurseOfElementsBuff::Effect(State* s) {
	OnEnd(s);
}

Event* CurseOfElementsBuff::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 300.f;
	e->buff = this;
	return e;
}

void CurseOfElementsBuff::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void CurseOfElementsBuff::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void CurseOfElementsBuff::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* CurseOfElementsBuff::Duplicate() {
	return new CurseOfElementsBuff();
}

void CurseOfElementsBuff::Clean() {}

bool BaneOfDoom::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void BaneOfDoom::Cast(State* s) {
	BaneOfDoomDoT* b = new BaneOfDoomDoT(*s->stats);
	s->AddBuff(b);
	if (s->HasBuff("BaneOfAgonyDoT")) {
		s->ClearEvents("BaneOfAgonyDoT");
		s->RemoveBuff("BaneOfAgonyDoT");
	}
	s->SetGCD();
}

void BaneOfDoom::Invoke(State* s, Stats& stats) {}

void BaneOfDoom::Arrive(State* s, Stats& stats) {}

void BaneOfDoomDoT::Effect(State* s) {
	float dmg = tick;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	CritEffect(crit_chance, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
		// Ebon Imp
	if (--ticks_left == 0) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
}

Event* BaneOfDoomDoT::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 15.f;
	e->buff = this;
	return e;
}

void BaneOfDoomDoT::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void BaneOfDoomDoT::OnReapplied(State* s) {
	++ticks_left;
	s->Relink(this);
}

void BaneOfDoomDoT::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* BaneOfDoomDoT::Duplicate() {
	return new BaneOfDoomDoT(this);
}

void BaneOfDoomDoT::Clean() {}

bool BaneOfAgony::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void BaneOfAgony::Cast(State* s) {
	s->AddBuff(new BaneOfAgonyDoT(*s->stats));
	if (s->HasBuff("BaneOfDoomDoT")) {
		s->ClearEvents("BaneOfDoomDoT");
		s->RemoveBuff("BaneOfDoomDoT");
	}
	s->SetGCD();
}

void BaneOfAgony::Invoke(State* s, Stats& stats) {}

void BaneOfAgony::Arrive(State* s, Stats& stats) {}

void BaneOfAgonyDoT::Effect(State* s) {
	float dmg = tick * mult;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	CritEffect(crit_chance, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	if (--ticks_left == 0) {
		switch (mult) {
		case 1:
			mult = 2;
			if (ticks_total > 13) ticks_left = 5;
			else ticks_left = 4;
			s->AddEvent(GetNextEvent(s));
			break;
		case 2:
			mult = 3;
			if (ticks_total > 12) ticks_left = 5;
			else ticks_left = 4;
			s->AddEvent(GetNextEvent(s));
			break;
		case 3:
			if (ticks_total == 16) {
				mult = 4;
				ticks_left = 1;
				s->AddEvent(GetNextEvent(s));
			}
			else OnEnd(s);
			break;
		case 4:
			OnEnd(s);
			break;
		}
	}
	else s->AddEvent(GetNextEvent(s));
}

Event* BaneOfAgonyDoT::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + tick_len;
	e->buff = this;
	return e;
}

void BaneOfAgonyDoT::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void BaneOfAgonyDoT::OnReapplied(State* s) {
	++ticks_total;
	if (ticks_total == 15) ++ticks_left;
	s->Relink(this);
}

void BaneOfAgonyDoT::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* BaneOfAgonyDoT::Duplicate() {
	return new BaneOfAgonyDoT(this);
}

void BaneOfAgonyDoT::Clean() {}

bool ShadowFlame::CanCast(State* s) {
	return !(s->HasBuff("ShadowFlameCD") || s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void ShadowFlame::Cast(State* s) {
	float dmg = (693 + s->stats->SpellDamage() * 0.1003f) * 1.15f * 1.02f * s->stats->bonusDamage;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(*s->stats, dmg);
	CritEffect(*s->stats, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	s->AddBuff(new ShadowFlameDoT(*s->stats));
	s->AddBuff(new ShadowFlameCD());
	s->SetGCD();
}

void ShadowFlame::Invoke(State* s, Stats& stats) {}

void ShadowFlame::Arrive(State* s, Stats& stats) {}

void ShadowFlameDoT::Effect(State* s) {
	float dmg = tick;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	CritEffect(*s->stats, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	if (--ticks_left == 0) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
}

Event* ShadowFlameDoT::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + tick_len;
	e->buff = this;
	return e;
}

void ShadowFlameDoT::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void ShadowFlameDoT::OnReapplied(State* s) {
	++ticks_left;
	s->Relink(this);
}

void ShadowFlameDoT::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* ShadowFlameDoT::Duplicate() {
	return new ShadowFlameDoT(this);
}

void ShadowFlameDoT::Clean() {}

void ShadowFlameCD::Effect(State* s) {
	OnEnd(s);
}

Event* ShadowFlameCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 12.f;
	e->buff = this;
	return e;
}

void ShadowFlameCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void ShadowFlameCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void ShadowFlameCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* ShadowFlameCD::Duplicate() {
	return new ShadowFlameCD();
}

void ShadowFlameCD::Clean() {}

bool HandOfGuldan::CanCast(State* s) {
	return !(s->HasBuff("HandOfGuldanCD") || s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void HandOfGuldan::Cast(State* s) {
	s->AddBuff(new Casting(CastTime(*s->stats, 2.f), this, s->stats));
}

void HandOfGuldan::Invoke(State* s, Stats& stats) {
	float dmg = (1532 + s->stats->SpellDamage() * 0.968f) * 1.15f * 1.02f * s->stats->bonusDamage;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(stats, dmg);
	CritEffect(stats, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	if (s->HasBuff("ImmolateDoT")) s->AddBuff(new ImmolateDoT(stats));
	s->AddBuff(new HandOfGuldanCD());
	s->AddBuff(new CurseOfGuldan());
	s->SetGCD();
	if (s->HasBuff("MetamorphosisCD")) {
		State* ns = s->Duplicate();
		((MetamorphosisCD*)(ns->HasBuff("MetamorphosisCD")))->SubCD(ns);
		s->AddFork(ns, 0.15f);
	}
}

void HandOfGuldan::Arrive(State* s, Stats& stats) {}

void HandOfGuldanCD::Effect(State* s) {
	OnEnd(s);
}

Event* HandOfGuldanCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 12.f;
	e->buff = this;
	return e;
}

void HandOfGuldanCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void HandOfGuldanCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void HandOfGuldanCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* HandOfGuldanCD::Duplicate() {
	return new HandOfGuldanCD();
}

void HandOfGuldanCD::Clean() {}

void CurseOfGuldan::Effect(State* s) {
	OnEnd(s);
}

Event* CurseOfGuldan::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 15.f;
	e->buff = this;
	return e;
}

void CurseOfGuldan::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void CurseOfGuldan::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void CurseOfGuldan::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* CurseOfGuldan::Duplicate() {
	return new CurseOfGuldan();
}

void CurseOfGuldan::Clean() {}

bool Corruption::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void Corruption::Cast(State* s) {
	s->AddBuff(new CorruptionDoT(*s->stats));
	s->SetGCD();
}

void Corruption::Invoke(State* s, Stats& stats) {}

void Corruption::Arrive(State* s, Stats& stats) {}

void CorruptionDoT::Effect(State* s) {
	float dmg = tick;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	CritEffect(crit_chance, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	if (--ticks_left == 0) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
	State* ns = s->Duplicate();
	ns->AddBuff(new ShadowTrance());
	s->AddFork(ns, 0.04f);
}

Event* CorruptionDoT::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + tick_len;
	e->buff = this;
	return e;
}

void CorruptionDoT::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void CorruptionDoT::OnReapplied(State* s) {
	++ticks_left;
	s->Relink(this);
}

void CorruptionDoT::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* CorruptionDoT::Duplicate() {
	return new CorruptionDoT(this);
}

void CorruptionDoT::Clean() {}

void ShadowTrance::Effect(State* s) {
	OnEnd(s);
}

Event* ShadowTrance::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 10.f;
	e->buff = this;
	return e;
}

void ShadowTrance::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void ShadowTrance::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void ShadowTrance::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* ShadowTrance::Duplicate() {
	return new ShadowTrance();
}

void ShadowTrance::Clean() {}

void ShadowTrance::Use(State* s) {
	s->ClearEvents(name);
	OnEnd(s);
}

bool SoulFire::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void SoulFire::Cast(State* s) {
	if (s->HasBuff("SoulburnBuff")) {
		s->ClearEvents("SoulburnBuff");
		s->RemoveBuff("SoulburnBuff");
		s->AddBuff(new Travelling(this, s->stats->distance * 0.1f, *s->stats));
	}
	else s->AddBuff(new Casting(CastTime(*s->stats, 3.f), this, s->stats));
}

void SoulFire::Invoke(State* s, Stats& stats) {
	s->AddBuff(new Travelling(this, stats.distance * 0.1f, stats));
}

void SoulFire::Arrive(State* s, Stats& stats) {
	float dmg = (2447 + stats.SpellDamage() * 0.726f) * 1.15f * 1.02f * stats.bonusDamage;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(stats, dmg);
	CritEffect(stats, dmg, s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f);
	s->AddDmg(dmg);
	if (s->HasBuff("MetamorphosisCD")) {
		State* ns = s->Duplicate();
		((MetamorphosisCD*)(ns->HasBuff("MetamorphosisCD")))->SubCD(ns);
		s->AddFork(ns, 0.15f);
	}
}

bool ShadowBite::CanCast(State* s) {
	return !s->HasBuff("ShadowBiteCD") && s->HasBuff("HunterPet");
}

void ShadowBite::Cast(State* s) {
	float dmg = (117 + s->stats->SpellDamage() * 1.1f * 0.6538f) * 1.15f * 1.103665 * (1.18 + s->stats->mastery * 2.3 / 17900.f);
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(*s->stats, dmg);
	CritEffect(*s->stats, dmg, (s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f) + (s->HasBuff("CurseOfGuldan") ? 0.1f : 0.f));
	s->AddDmg(dmg);
	s->AddBuff(new ShadowBiteCD());
	return;
}

void ShadowBite::Invoke(State* s, Stats& stats) {}

void ShadowBite::Arrive(State* s, Stats& stats) {}

void ShadowBiteCD::Effect(State* s) {
	OnEnd(s);
}

Event* ShadowBiteCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 6.f;
	e->buff = this;
	return e;
}

void ShadowBiteCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void ShadowBiteCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void ShadowBiteCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* ShadowBiteCD::Duplicate() {
	return new ShadowBiteCD();
}

void ShadowBiteCD::Clean() {}

bool HunterMelee::CanCast(State* s) {
	return !s->HasBuff("HunterMeleeCD") && s->HasBuff("HunterPet");
}

void HunterMelee::Cast(State* s) {
	float dmg = (606 + s->stats->SpellDamage() * 0.1065f) * (1.18 + s->stats->mastery * 2.3 / 17900.f);
	MissEffect(*s->stats, dmg);
	CritEffect(*s->stats, dmg, s->HasBuff("CurseOfGuldan") ? 0.1f : 0.f);
	s->AddDmg(dmg);
	s->AddBuff(new HunterMeleeCD());
}

void HunterMelee::Invoke(State* s, Stats& stats) {}

void HunterMelee::Arrive(State* s, Stats& stats) {}

void HunterMeleeCD::Effect(State* s) {
	OnEnd(s);
}

Event* HunterMeleeCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 2.f / (1 + s->stats->haste / 12800.f);
	e->buff = this;
	return e;
}

void HunterMeleeCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void HunterMeleeCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void HunterMeleeCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* HunterMeleeCD::Duplicate() {
	return new HunterMeleeCD();
}

void HunterMeleeCD::Clean() {}

bool GuardMelee::CanCast(State* s) {
	return !s->HasBuff("GuardMeleeCD") && s->HasBuff("GuardPet");
}

void GuardMelee::Cast(State* s) {
	float dmg = (861 + s->stats->SpellDamage() * 0.1188) * (1.18 + s->stats->mastery * 2.3 / 17900.f);
	MissEffect(*s->stats, dmg);
	CritEffect(*s->stats, dmg, s->HasBuff("CurseOfGuldan") ? 0.1f : 0.f);
	s->AddDmg(dmg);
	s->AddBuff(new GuardMeleeCD());
}

void GuardMelee::Invoke(State* s, Stats& stats) {}

void GuardMelee::Arrive(State* s, Stats& stats) {}

void GuardMeleeCD::Effect(State* s) {
	OnEnd(s);
}

Event* GuardMeleeCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 2.f / (1 + s->stats->haste / 12800.f);
	e->buff = this;
	return e;
}

void GuardMeleeCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void GuardMeleeCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void GuardMeleeCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* GuardMeleeCD::Duplicate() {
	return new GuardMeleeCD();
}

void GuardMeleeCD::Clean() {}

void GuardPet::Effect(State* s) {}

Event* GuardPet::GetNextEvent(State* s) { return nullptr; }

void GuardPet::OnStart(State* s) {
	s->RemoveBuff("HunterPet");
}

void GuardPet::OnReapplied(State* s) {}

void GuardPet::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* GuardPet::Duplicate() {
	return new GuardPet();
}

void GuardPet::Clean() {}

void HunterPet::Effect(State* s) {}

Event* HunterPet::GetNextEvent(State* s) { return nullptr; }

void HunterPet::OnStart(State* s) {
	s->RemoveBuff("GuardPet");
}

void HunterPet::OnReapplied(State* s) {}

void HunterPet::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* HunterPet::Duplicate() {
	return new HunterPet();
}

void HunterPet::Clean() {}

bool LegionStrike::CanCast(State* s) {
	return !s->HasBuff("LegionStrikeCD") && s->HasBuff("GuardPet");
}

void LegionStrike::Cast(State* s) {
	float dmg = (917 + s->stats->SpellDamage() * 1.1f * 0.358f) * 1.15f * 1.12757 * (1.18 + s->stats->mastery * 2.3 / 17900.f);
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(*s->stats, dmg);
	CritEffect(*s->stats, dmg, (s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f) + (s->HasBuff("CurseOfGuldan") ? 0.1f : 0.f));
	s->AddDmg(dmg);
	s->AddBuff(new LegionStrikeCD());
}

void LegionStrike::Invoke(State* s, Stats& stats) {}

void LegionStrike::Arrive(State* s, Stats& stats) {}

void LegionStrikeCD::Effect(State* s) {
	OnEnd(s);
}

Event* LegionStrikeCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 6.f;
	e->buff = this;
	return e;
}

void LegionStrikeCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void LegionStrikeCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void LegionStrikeCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* LegionStrikeCD::Duplicate() {
	return new LegionStrikeCD();
}

void LegionStrikeCD::Clean() {}

bool FelStorm::CanCast(State* s) {
	return !s->HasBuff("FelStormCD") && s->HasBuff("GuardPet");
}

void FelStorm::Cast(State* s) {
	s->AddBuff(new FelStormDoT(*s->stats));
	s->AddBuff(new FelStormCD());
	s->SetGCD();
}

void FelStorm::Invoke(State* s, Stats& stats) {}

void FelStorm::Arrive(State* s, Stats& stats) {}

void FelStormCD::Effect(State* s) {
	OnEnd(s);
}

Event* FelStormCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 45.f;
	e->buff = this;
	return e;
}

void FelStormCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void FelStormCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void FelStormCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* FelStormCD::Duplicate() {
	return new FelStormCD();
}

void FelStormCD::Clean() {}

void FelStormDoT::Effect(State* s) {
	float dmg = tick;
	if (s->HasBuff("CurseOfElements")) dmg *= 1.08f;
	MissEffect(*s->stats, dmg);
	CritEffect(crit_chance, dmg, (s->HasBuff("ShadowAndFlame") ? 0.05f : 0.f) + (s->HasBuff("CurseOfGuldan") ? 0.1f : 0.f));
	s->AddDmg(dmg);
	if (--ticks_left == 0) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
}

Event* FelStormDoT::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 1.f;
	e->buff = this;
	return e;
}

void FelStormDoT::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void FelStormDoT::OnReapplied(State* s) {
	s->Relink(this);
}

void FelStormDoT::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* FelStormDoT::Duplicate() {
	return new FelStormDoT(this);
}

void FelStormDoT::Clean() {}

void Soulshard::Effect(State* s) {}

Event* Soulshard::GetNextEvent(State* s) { return nullptr; }

void Soulshard::OnStart(State* s) {}

void Soulshard::OnReapplied(State* s) {}

void Soulshard::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* Soulshard::Duplicate() {
	return new Soulshard(this);
}

void Soulshard::Clean() {}

bool Soulburn::CanCast(State* s) {
	return !s->HasBuff("Casting") && s->HasBuff("Soulshard");
}

void Soulburn::Cast(State* s) {
	s->AddBuff(new SoulburnBuff());
	s->AddBuff(new TemporalRuin());
	((Soulshard*)(s->HasBuff("Soulshard")))->UseShard(s);
}

void Soulburn::Invoke(State* s, Stats& stats) {}

void Soulburn::Arrive(State* s, Stats& stats) {}

void SoulburnBuff::Effect(State* s) {
	OnEnd(s);
}

Event* SoulburnBuff::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 15.f;
	e->buff = this;
	return e;
}

void SoulburnBuff::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void SoulburnBuff::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void SoulburnBuff::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* SoulburnBuff::Duplicate() {
	return new SoulburnBuff();
}

void SoulburnBuff::Clean() {}

void TemporalRuin::Effect(State* s) {
	OnEnd(s);
}

Event* TemporalRuin::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 10.f;
	e->buff = this;
	return e;
}

void TemporalRuin::OnStart(State* s) {
	s->stats->bonusSpellpower += 0.1;
	s->AddEvent(GetNextEvent(s));
}

void TemporalRuin::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void TemporalRuin::OnEnd(State* s) {
	s->stats->bonusSpellpower -= 0.1;
	s->RemoveBuff(name);
}

Buff* TemporalRuin::Duplicate() {
	return new TemporalRuin();
}

void TemporalRuin::Clean() {}

void SoulburnCD::Effect(State* s) {
	OnEnd(s);
}

Event* SoulburnCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 45.f;
	e->buff = this;
	return e;
}

void SoulburnCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void SoulburnCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void SoulburnCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* SoulburnCD::Duplicate() {
	return new SoulburnCD();
}

void SoulburnCD::Clean() {}

bool DemonSoul::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->HasBuff("DemonSoulCD")) && (s->HasBuff("GuardPet") || s->HasBuff("HunterPet"));
}

void DemonSoul::Cast(State* s) {
	s->AddBuff(new DemonSoulBuff());
	s->AddBuff(new DemonSoulCD());
}

void DemonSoul::Invoke(State* s, Stats& stats) {}

void DemonSoul::Arrive(State* s, Stats& stats) {}

void DemonSoulCD::Effect(State* s) {
	OnEnd(s);
}

Event* DemonSoulCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 120.f;
	e->buff = this;
	return e;
}

void DemonSoulCD::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void DemonSoulCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void DemonSoulCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* DemonSoulCD::Duplicate() {
	return new DemonSoulCD();
}

void DemonSoulCD::Clean() {}

void DemonSoulBuff::Effect(State* s) {
	OnEnd(s);
}

Event* DemonSoulBuff::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 20.f;
	e->buff = this;
	return e;
}

void DemonSoulBuff::OnStart(State* s) {
	if (s->HasBuff("GuardPet")) {
		demon = 'g';
		s->stats->bonusHaste *= 1.15f;
		s->stats->bonusDamage *= 1.1f;
	} else {
		demon = 'h';
		s->stats->bonusDoT = 1.2f;
	}
	s->AddEvent(GetNextEvent(s));
}

void DemonSoulBuff::OnReapplied(State* s) {
	if (s->HasBuff("GuardPet")) {
		demon = 'g';
	} else {
		demon = 'h';
	}
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void DemonSoulBuff::OnEnd(State* s) {
	if (demon == 'g') {
		s->stats->bonusHaste /= 1.15f;
		s->stats->bonusDamage /= 1.1f;
	} else {
		s->stats->bonusDoT = 1.f;
	}
	s->RemoveBuff(name);
}

Buff* DemonSoulBuff::Duplicate() {
	return new DemonSoulBuff(this);
}

void DemonSoulBuff::Clean() {}

bool Metamorphosis::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->HasBuff("MetamorphosisCD"));
}

void Metamorphosis::Cast(State* s) {
	s->AddBuff(new MetamorphosisBuff());
	s->AddBuff(new MetamorphosisCD());
}

void Metamorphosis::Invoke(State* s, Stats& stats) {}

void Metamorphosis::Arrive(State* s, Stats& stats) {}

void MetamorphosisCD::Effect(State* s) {
	OnEnd(s);
}

Event* MetamorphosisCD::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = expiration;
	e->buff = this;
	return e;
}

void MetamorphosisCD::OnStart(State* s) {
	expiration = s->GetTime() + 180.f;
	s->AddEvent(GetNextEvent(s));
}

void MetamorphosisCD::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void MetamorphosisCD::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* MetamorphosisCD::Duplicate() {
	return new MetamorphosisCD(expiration);
}

void MetamorphosisCD::Clean() {}

void MetamorphosisCD::SubCD(State* s) {
	expiration -= 15.f;
	s->ClearEvents(name);
	if (expiration <= s->GetTime()) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
}

void MetamorphosisBuff::Effect(State* s) {
	OnEnd(s);
}

Event* MetamorphosisBuff::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 36.f;
	e->buff = this;
	return e;
}

void MetamorphosisBuff::OnStart(State* s) {
	bonusDmg = 1.38f + s->stats->mastery * 2.3f / 17900.f;
	s->stats->bonusDamage *= bonusDmg;
	s->AddEvent(GetNextEvent(s));
}

void MetamorphosisBuff::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->HasBuff("Metamorphosis")->OnEnd(s);
	s->AddEvent(GetNextEvent(s));
}

void MetamorphosisBuff::OnEnd(State* s) {
	s->stats->bonusDamage /= bonusDmg;
	s->RemoveBuff(name);
}

Buff* MetamorphosisBuff::Duplicate() {
	return new MetamorphosisBuff(this);
}

void MetamorphosisBuff::Clean() {}

bool SummonHunter::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void SummonHunter::Cast(State* s) {
	if (s->HasBuff("SoulburnBuff")) {
		s->RemoveBuff("GuardPet");
		if (s->HasBuff("Felstorm")) {
			s->ClearEvents("Felstorm");
			s->HasBuff("Felstorm")->OnEnd(s);
		}
		s->AddBuff(new HunterPet());
	} else s->AddBuff(new Casting(CastTime(*s->stats, 5.f), this, s->stats));
	s->SetGCD();
}

void SummonHunter::Invoke(State* s, Stats& stats) {
	s->RemoveBuff("GuardPet");
	if (s->HasBuff("Felstorm")) {
		s->ClearEvents("Felstorm");
		s->HasBuff("Felstorm")->OnEnd(s);
	}
	s->AddBuff(new HunterPet());
}

void SummonHunter::Arrive(State* s, Stats& stats) {}

bool SummonGuard::CanCast(State* s) {
	return !(s->HasBuff("Casting") || s->GetGCD() > 0.f);
}

void SummonGuard::Cast(State* s) {
	if (s->HasBuff("SoulburnBuff")) {
		s->RemoveBuff("HunterPet");
		s->AddBuff(new GuardPet());
	}
	else s->AddBuff(new Casting(CastTime(*s->stats, 5.f), this, s->stats));
	s->SetGCD();
}

void SummonGuard::Invoke(State* s, Stats& stats) {
	s->RemoveBuff("HunterPet");
	s->AddBuff(new GuardPet());
}

void SummonGuard::Arrive(State* s, Stats& stats) {}