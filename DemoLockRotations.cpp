#include "DemoLock.hpp"


long DemoLockRecursive(State* s, Spell** spells, float maxTime) {
	if (s->GetTime() > maxTime) {
		return 0L;
	}
	bool canCast = false;
	long max = s->GetDmg();
	while (!canCast) {
		if (spells[HUNTER_MELEE]->CanCast(s)) spells[HUNTER_MELEE]->Cast(s);
		if (spells[SHADOW_BITE]->CanCast(s)) spells[SHADOW_BITE]->Cast(s);
		if (spells[LEGION_STRIKE]->CanCast(s)) spells[LEGION_STRIKE]->Cast(s);
		if (spells[GUARD_MELEE]->CanCast(s)) spells[GUARD_MELEE]->Cast(s);
		if (spells[FELSTORM]->CanCast(s)) spells[FELSTORM]->Cast(s);
		for (int i = 0; i < DEMO_SPELL_COUNT; ++i) {
			if (spells[i]->CanCast(s)) {
				if (i == HUNTER_MELEE || i == SHADOW_BITE || i == LEGION_STRIKE || i == GUARD_MELEE || i == FELSTORM) {
					continue;
					//spells[i]->Cast(s);
				} else {
					canCast = true;
					State* dup = s->Duplicate();
					spells[i]->Cast(dup);
					long n = DemoLockRecursive(dup, spells, maxTime);
					if (n > max) max = n;
					dup->Clean();
					delete dup;
				}
			}
		}
		Event* e = s->GetEvent();
		if (e != nullptr) delete e;
		if (s->GetTime() > maxTime) {
			if (s->HasFork()) {
				State* fork = s->GetFork();
				fork->Clean();
				delete fork;
			}
			return max;
		}
		if (max < s->GetDmg()) max = s->GetDmg();
		if (s->HasFork()) {
			State* fork = s->GetFork();
			float p = s->GetProbability();
			long n = p * DemoLockRecursive(fork, spells, maxTime) + (1 - p) * DemoLockRecursive(s, spells, maxTime);
			if (n > max) max = n;
			fork->Clean();
			delete fork;
			return max;
		}
	}
	if (!s->NoEvents()) {
		long n = DemoLockRecursive(s, spells, maxTime);
		if (n > max) max = n;
	}

	return max;
}

bool SmartDemoLockOpener(State* s, Spell** spells, float maxTime) {
	if (!(s->HasBuff("MetamorphosisCD") || s->HasBuff("DemonSoulCD") || s->HasBuff("SoulburnCD"))) {
		if (!s->HasBuff("GuardPet")) {
			if (spells[SUMMON_GUARD]->CanCast(s)) spells[SUMMON_GUARD]->Cast(s);
			return true;
		}
		if (spells[SOULBURN]->CanCast(s)) spells[SOULBURN]->Cast(s);
		if (spells[DEMON_SOUL]->CanCast(s)) spells[DEMON_SOUL]->Cast(s);
		return true;
	}
	if (!s->HasBuff("MetamorphosisCD") && !s->HasBuff("SoulburnBuff") && s->HasBuff("DemonSoulBuff") && !s->HasBuff("SoulburnCD")) {
		if (spells[SOULBURN]->CanCast(s)) spells[SOULBURN]->Cast(s);
		else return true;
	}
	if (!s->HasBuff("MetamorphosisCD") && s->HasBuff("SoulburnBuff") && !s->HasBuff("DemonSoulBuff") && !s->HasBuff("DemonSoulCD")) {
		if (spells[DEMON_SOUL]->CanCast(s)) spells[DEMON_SOUL]->Cast(s);
		else return true;
	}
	if (s->HasBuff("SoulburnBuff") && s->HasBuff("DemonSoulBuff") && !s->HasBuff("MetamorphosisCD")) {
		ImmolateDoT* im = (ImmolateDoT*)(s->HasBuff("ImmolateDoT"));
		if (!im || im->Ticks_left() < 3) {
			if (spells[IMMOLATE]->CanCast(s)) spells[IMMOLATE]->Cast(s);
			return true;
		} else {
			if (spells[METAMORPHOSIS]->CanCast(s)) spells[METAMORPHOSIS]->Cast(s);
			else return true;
		}
	}
	if (s->HasBuff("SoulburnBuff") && s->HasBuff("DemonSoulBuff") && s->HasBuff("MetamorphosisBuff")) {
		BaneOfDoomDoT* bot = (BaneOfDoomDoT*)(s->HasBuff("BaneOfDoomDoT"));
		if (!bot || bot->Tick() < (1947 + 0.88f * s->stats->SpellDamage()) * 1.15f * 1.02f * s->stats->bonusDamage * s->stats->bonusDoT) {
			if (spells[BANE_OF_DOOM]->CanCast(s)) spells[BANE_OF_DOOM]->Cast(s);
			return true;
		}
		CorruptionDoT* co = (CorruptionDoT*)(s->HasBuff("CorruptionDoT"));
		if (!co || co->Tick() < (1947 + 0.88f * s->stats->SpellDamage()) * 1.15f * 1.02f * s->stats->bonusDamage * s->stats->bonusDoT) {
			if (spells[BANE_OF_DOOM]->CanCast(s)) spells[BANE_OF_DOOM]->Cast(s);
			return true;
		}
		if (spells[SHADOWFLAME]->CanCast(s)) {
			spells[SHADOWFLAME]->Cast(s);
			return true;
		} else {
			if (spells[SUMMON_HUNTER]->CanCast(s)) spells[SUMMON_HUNTER]->Cast(s);
			return true;
		}
	}
	return false;
}

long SmartDemoLock(State* s, Spell** spells, float maxTime) {
	if (s->GetTime() > maxTime) {
		return 0L;
	}
	bool canCast = false;
	long max = s->GetDmg();
	while (!canCast) {

		
		Event* e = s->GetEvent();
		if (e != nullptr) delete e;
		if (s->GetTime() > maxTime) {
			if (s->HasFork()) {
				State* fork = s->GetFork();
				fork->Clean();
				delete fork;
			}
			return max;
		}
		if (max < s->GetDmg()) max = s->GetDmg();
		if (s->HasFork()) {
			State* fork = s->GetFork();
			float p = s->GetProbability();
			long n = p * DemoLockRecursive(fork, spells, maxTime) + (1 - p) * DemoLockRecursive(s, spells, maxTime);
			if (n > max) max = n;
			fork->Clean();
			delete fork;
			return max;
		}
		if (s->NoEvents()) return 0L;
	}

	return max;
}