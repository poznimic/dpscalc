/*
 * Authored by Teedit
 * Created July 2020
 * Developed to prove Barbara's fault in her rotation.
*/

#include "Core.hpp"
#include <iostream>

RNG* RNG::instance = 0;

Event* State::GetEvent() {
	if (q.empty()) return nullptr;
	Event* ret = q.top();
	q.pop();
	float d = gcd - (ret->time - time);
	if (d < 0) gcd = 0;
	else gcd = d;
	time = ret->time;
	if (ret->buff != nullptr) ret->buff->Effect(this);
	return ret;
}

void State::AddBuff(Buff* b) {
	if (b->overwrites && HasBuff(b->name)) {
		b->OnReapplied(this);
		RemoveBuff(b->name);
		buffs.push_back(b);
		return;
	}
	b->OnStart(this);
	buffs.push_back(b);
}

Buff* State::HasBuff(std::string name) {
	for (Buff* b : buffs) {
		if (b->name._Equal(name)) return b;
	}
	return nullptr;
}

void State::RemoveBuff(std::string name) {
	auto it = buffs.begin();
	while (it != buffs.end()) {
		if ((*it)->name._Equal(name)) {
			(*it)->Clean();
			delete *it;
			buffs.erase(it);
			return;
		}
		++it;
	}
}

void State::ClearEvents(std::string name) {
	std::priority_queue<Event*, std::vector<Event*>, EventComparator> tmpq;
	while (!q.empty()) {
		Event* e = q.top();
		q.pop();
		if (e->buff && !e->buff->name._Equal(name)) tmpq.push(e);
		else delete e;
	}
	q.swap(tmpq);
}

void State::Relink(Buff* buff) {
	std::priority_queue<Event*, std::vector<Event*>, EventComparator> tmpq;
	while (!q.empty()) {
		Event* e = q.top();
		if (e->buff && e->buff->name._Equal(buff->name))
			e->buff = buff;
		tmpq.push(e);
		q.pop();
	}
	q.swap(tmpq);
}

State* State::Duplicate() {
	State* ret = new State();
	ret->time = time;
	ret->dmg = dmg;
	ret->gcd = gcd;
	*(ret->stats) = stats;
	std::priority_queue<Event*, std::vector<Event*>, EventComparator> temp;

	for (Buff* b : buffs) {
		ret->buffs.push_back(b->Duplicate());
	}

	while (!q.empty()) {
		Event* e = q.top();
		q.pop();
		temp.push(e);
		Event* dup = new Event();
		dup->time = e->time;
		if (e->buff == nullptr) dup->buff = nullptr;
		else {
			auto bufit = buffs.begin();
			auto dupit = ret->buffs.begin();
			while ((*bufit) != e->buff) {
				++bufit; ++dupit;
			}
			dup->buff = *dupit;
		}
		ret->q.push(dup);
	}
	q.swap(temp);
	return ret;
}

void State::Clean() {
	delete stats;
	while (!q.empty()) {
		Event* e = q.top();
		q.pop();
		delete e;
	}
	for (Buff* b : buffs) {
		b->Clean();
		delete b;
	}
}

void TestBuff::Effect(State* s) {
	OnEnd(s);
}

Event* TestBuff::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 15.f;
	e->buff = this;
	return e;
}

void TestBuff::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void TestBuff::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void TestBuff::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* TestBuff::Duplicate() {
	return new TestBuff();
}

void TestBuff::Clean() {}

void TestDebuff::Effect(State* s) {
	s->AddDmg(s->HasBuff("TestProc") ? 33 : 22);
	if (RNG::GetInstance()->Number() == 0) s->AddBuff(new TestProc());
	if (++ticks == 6 || (ticks == 5 && !reapplied)) OnEnd(s);
	else s->AddEvent(GetNextEvent(s));
}

Event* TestDebuff::GetNextEvent(State* s) {
	Event* e = new Event();
	e->buff = this;
	e->time = s->GetTime() + 3;
	return e;
}

void TestDebuff::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void TestDebuff::OnReapplied(State* s) {
	reapplied = true;
}

void TestDebuff::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* TestDebuff::Duplicate() {
	TestDebuff* ret = new TestDebuff();
	ret->ticks = ticks;
	ret->reapplied = reapplied;
	return ret;
}

void TestDebuff::Clean() {}

void TestProc::Effect(State* s) {
	OnEnd(s);
}

Event* TestProc::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + 10.f;
	e->buff = this;
	return e;
}

void TestProc::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void TestProc::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void TestProc::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* TestProc::Duplicate() {
	return new TestProc();
}

void TestProc::Clean() {}

void Casting::Effect(State* s) {
	spell->Invoke(s, stats);
	OnEnd(s);
}

Event* Casting::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + duration;
	e->buff = this;
	return e;
}

void Casting::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void Casting::OnReapplied(State* s) {
	s->ClearEvents(name);
	s->AddEvent(GetNextEvent(s));
}

void Casting::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* Casting::Duplicate() {
	return new Casting(duration, spell, &stats);
}

void Casting::Clean() {}

void Travelling::Effect(State* s) {
	spell->Arrive(s, _stats);
	OnEnd(s);
}

Event* Travelling::GetNextEvent(State* s) {
	Event* e = new Event();
	e->time = s->GetTime() + duration;
	e->buff = this;
	return e;
}

void Travelling::OnStart(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void Travelling::OnReapplied(State* s) {
	s->AddEvent(GetNextEvent(s));
}

void Travelling::OnEnd(State* s) {
	s->RemoveBuff(name);
}

Buff* Travelling::Duplicate() {
	return new Travelling(spell, duration, _stats);
}

void Travelling::Clean() {}